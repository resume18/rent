import datetime
import os.path
from functools import lru_cache
from typing import List, Optional

from pydantic import BaseSettings


class Settings(BaseSettings):
    APP_HOST: str = "localhost"
    APP_PORT: int = 8000
    SECRET_KEY: str = "key"
    SERVE_FILES_LOCALLY: bool = False
    PASSWORD_ENCRYPTION_ALGORITHM: str = "HS256"
    ACCESS_TOKEN_EXPIRES_DAYS: int = 30
    DSN__DB = 'postgresql+asyncpg://admin:1111@db:5432/airbnb'
    UPLOAD_DIR: str = "media"
    BASE_DIR: str = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    GOOGLE_OAUTH2_CLIENT_ID: str = ""
    GOOGLE_OAUTH2_CLIENT_SECRET: str = ""
    DEBUG: bool = False
    CORS_ALLOWED_ORIGINS: Optional[List[str]]

    if not os.path.exists(UPLOAD_DIR):
        os.mkdir(UPLOAD_DIR)

    # Settings for testing:
    TEST_DB_NAME: str = "test_airbnb"


@lru_cache()
def get_settings():
    return Settings()
