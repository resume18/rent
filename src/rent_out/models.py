from datetime import datetime, timedelta

from src.db import Base
from sqlalchemy.orm import relationship, backref
from sqlalchemy import Integer, Numeric, Boolean, ForeignKey, Column, DateTime, Index

from src.settings import get_settings


class Deal(Base):
    TIME_FOR_REVIEW_AND_GRADE = timedelta(days=14)

    __tablename__ = "deal"

    id = Column(Integer, primary_key=True, autoincrement=True)
    total_cost = Column(Numeric, nullable=False)
    is_success = Column(Boolean)
    entry = Column(DateTime, nullable=False)
    departure = Column(DateTime, nullable=False)
    ad_id = Column(
        Integer,
        ForeignKey("ad.id", ondelete="CASCADE", onupdate="CASCADE"),
        nullable=False,
    )
    renter_id = Column(
        Integer,
        ForeignKey("user.id", ondelete="CASCADE", onupdate="CASCADE"),
        nullable=False,
    )
    tenant_id = Column(
        Integer,
        ForeignKey("user.id", ondelete="CASCADE", onupdate="CASCADE"),
        nullable=False,
    )
    payment_time = Column(DateTime)
    return_money_time = Column(DateTime)

    tenant = relationship(
        "User",
        backref=backref("deal", cascade="all, delete-orphan, save-update"),
        foreign_keys=[tenant_id],
    )
    ad = relationship(
        "Ad",
        backref=backref("deal", cascade="all, delete-orphan, save-update"),
        foreign_keys=[ad_id],
    )

    def check_timeout(self):
        settings = get_settings()
        return self.created_at + settings.TIME_FOR_REVIEW_AND_GRADE > datetime.now()


deal_renter_index = Index("deal_renter_index", Deal.renter_id)
dela_tenant_index = Index("deal_tenant_index", Deal.tenant_id)
