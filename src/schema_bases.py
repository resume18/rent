import json
from typing import Optional, Tuple, Dict, Any, Type, Container

from pydantic import BaseModel, BaseConfig
from pydantic.main import ModelMetaclass, create_model
from sqlalchemy import inspect
from sqlalchemy.orm import ColumnProperty


class BaseModelWithORM(BaseModel):
    class Config(BaseConfig):
        orm_mode = True


class BaseWithReadDumpedJSON(BaseModel):
    """
    Need for parse json string from multipart.
    """

    @classmethod
    def validate(cls, value):
        if isinstance(value, str):
            value = json.loads(value)
        return super().validate(value)


class AllOptionalMeta(ModelMetaclass):
    def __new__(mcs, name: str, bases: Tuple[type], namespaces: Dict[str, Any], **kwargs):
        annotations = {}
        for base in bases:
            for base_ in base.__mro__:
                if base_ is BaseModel:
                    break
                if hasattr(base_, "__annotations__"):
                    annotations.update(base_.__annotations__)

        annotations.update(namespaces.get("__annotations__", {}))

        for field in annotations:
            if not field.startswith("__"):
                annotations[field] = Optional[annotations[field]]

        namespaces["__annotations__"] = annotations

        return super().__new__(mcs, name, bases, namespaces, **kwargs)


def sqlalchemy_to_pydantic(
    db_model: Type,
    model_name: str,
    *,
    config: Type = None,
    exclude: Container[str] = (),
    include: Container[str] = (),
    base: Type = None,
) -> Type[BaseModel]:
    """
    Changes:
    :param include: Fields that must be in the schema.
    :param model_name: Schema name.

    The fact is that if we map the same model several
    times for different schemas, the original function
    will create a pydantic model with the same name -
    the name of the sqlalchemy model,
    and an exception will occur in FastAPI.
    Source: https://github.com/tiangolo/pydantic-sqlalchemy
    """
    mapper = inspect(db_model)
    fields = {}
    assert not (include and exclude), (
        f"Cannot set both 'include' and 'exclude' options on " "schema {model_name}."
    )
    for attr in mapper.attrs:
        if isinstance(attr, ColumnProperty):
            if attr.columns:
                name = attr.key
                if (include and name not in include) or (exclude and name in exclude):
                    continue
                column = attr.columns[0]
                python_type: Optional[type] = None
                if hasattr(column.type, "impl"):
                    if hasattr(column.type.impl, "python_type"):
                        python_type = column.type.impl.python_type
                elif hasattr(column.type, "python_type"):
                    python_type = column.type.python_type
                assert python_type, f"Could not infer python_type for {column}"
                default = None
                if column.default is None and not column.nullable:
                    default = ...
                fields[name] = (python_type, default)
    pydantic_model = create_model(model_name, __config__=config, __base__=base, **fields)  # type: ignore
    return pydantic_model
