import logging

from sqlalchemy.orm import sessionmaker
from asyncio import current_task
from src.settings import get_settings
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession, async_scoped_session


settings = get_settings()

engine = create_async_engine(settings.DSN__DB)


if settings.DEBUG:
    logging.basicConfig()
    logging.getLogger("sqlalchemy.engine").setLevel(logging.INFO)

session = async_scoped_session(
    sessionmaker(bind=engine, autoflush=False, autocommit=False, class_=AsyncSession, expire_on_commit=False),
    current_task
)

Base = declarative_base()


async def get_db():
    db = session()
    try:
        yield db
    finally:
        await db.close()
