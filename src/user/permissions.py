from fastapi import Depends
from fastapi_permissions import (
    Deny,
    Allow,
    Authenticated,
    Everyone,
    configure_permissions,
)

from src.user.jwt import verify_token
from src.user.models import User


def user_grade_acl(user_id: int):
    return [
        (Deny, f"user:{user_id}", "create"),
        (Allow, Authenticated, "create"),
    ]


def user_review_acl(user_id: int):
    return [
        (Deny, f"user:{user_id}", "create"),
        (Allow, Authenticated, "create"),
    ]


def user_acl(user_id: int):
    return [(Allow, f"user:{user_id}", "edit"), (Allow, f"user:{user_id}", "delete")]


def get_user_principals(user: User = Depends(verify_token)):
    return [
        f"user:{user.id}",
        f"user:{user.is_staff}",
        f"user:{user.is_superuser}",
        Authenticated,
        Everyone,
    ]


UserPrincipals = configure_permissions(get_user_principals)
