import json
from .factories import UserFactory, create_auth_user


def test_register_user(client):
    user = UserFactory()
    data = {"username": user.username, "password": user.password}
    response = client.post("/register", json.dumps(data))
    assert response.status_code == 200


def test_login(client):
    user = UserFactory()
    data = {"username": user.username, "password": user.password}
    register_response = client.post("/register", json.dumps(data))
    response = client.post("/login", data)
    assert response.status_code == 200


def test_post_messages(client):
    sender = create_auth_user(client)
    receiver = create_auth_user(client)
    data = {"text": "asdasdd"}
    with client.websocket_connect(
        f'/chat/{receiver["id"]}?token={sender["token"]}'
    ) as websocket:
        websocket.send_json(data)
        websocket.close()


def test_get_messages(client):
    sender = create_auth_user(client)
    receiver = create_auth_user(client)
    response = client.get(f'chat/{receiver["id"]}', headers=sender["headers"])
    assert response.status_code == 200


def test_get_profile(client):
    user = create_auth_user(client)
    response = client.get(f"/profile/{user['id']}", headers=user["headers"])
    assert response.status_code == 200


def test_get_reviews(client):
    user = create_auth_user(client)
    response = client.get(f"/profile/{user['id']}/review", headers=user["headers"])
    assert response.status_code == 200


def test_create_user_review(client):
    user = create_auth_user(client)
    user2 = create_auth_user(client)
    data = {"text": "sdffsf"}
    response = client.post(
        f"/profile/{user['id']}/review", json.dumps(data), headers=user2["headers"]
    )
    assert response.status_code == 200


def test_create_user_grade(client):
    user = create_auth_user(client)
    user2 = create_auth_user(client)
    data = {"grade": True}
    response = client.post(
        f"/profile/{user['id']}/grade", json.dumps(data), headers=user2["headers"]
    )
    assert response.status_code == 200
