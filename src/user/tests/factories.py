import json

from factory import Factory, Faker, Sequence
from ..models import User


class UserFactory(Factory):
    class Meta:
        model = User

    username = Sequence(lambda n: "+79196%06d" % n)
    password = Faker("password")


def create_auth_user(client) -> dict:
    user = UserFactory()
    data = {"username": user.username, "password": user.password}
    register_response = client.post("/register", json.dumps(data)).json()
    response = client.post("/login", data).json()
    register_response["token"] = response["access_token"]
    register_response["headers"] = {"Authorization": f"bearer {response['access_token']}"}
    register_response["password"] = user.password
    return register_response
