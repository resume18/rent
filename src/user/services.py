from fastapi import UploadFile, HTTPException
from fastapi.security import OAuth2PasswordRequestForm
from passlib.exc import InvalidTokenError
from sqlalchemy import select, insert
from sqlalchemy.exc import IntegrityError, NoResultFound
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import Session
from starlette import status

from .hashing import get_password_hash
from .schema import (
    RegisterSchema,
    UserUpdateSchema,
    UserGradeCreateSchema,
    UserReviewCreateSchema,
)
from .models import User, Message, UserGrade, UserReview
from src.exceptions import ModelNotFoundError
from ..settings import Settings
from typing import List
from ..utils.utils import parse_obj_to_model, save_file, delete_file


async def user_registration(database: AsyncSession, data: RegisterSchema) -> User:
    try:
        stmt = insert(User).values(username=data.username, password=get_password_hash).returning()
        result = await database.execute(stmt)
        await database.commit()
    except IntegrityError:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="User already exists.",
        )
    else:
        return result


async def oauth2_registration(db: AsyncSession, email: str) -> User:
    try:
        new_user = User(email=email)
        db.add(new_user)
        await db.commit()
    except IntegrityError:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="User already exists."
        )
    return new_user


async def get_user_by_id(database: AsyncSession, user_id: int) -> User:
    stmt = select(User).where(User.id == user_id)
    r = await database.execute(stmt)
    user = await r.first()
    return user


def verify_credentials(database: Session, payload: OAuth2PasswordRequestForm) -> User:
    user = database.query(User).filter(User.username == payload.username).one()
    if not user.check_password(payload.password):
        raise InvalidTokenError
    return user


def get_user_by_email(database: Session, email: str) -> User:
    user = database.query(User).filter(User.email == email).first()
    return user


def create_text_message(
    database: Session, text: str, sender_id: int, receiver_id: int
) -> None:
    get_user_by_id(database, receiver_id)
    message = Message(text=text, sender_id=sender_id, receiver_id=receiver_id)
    database.add(message)
    database.commit()


async def edit_user(
    db: Session,
    settings: Settings,
    updated_user_schema: UserUpdateSchema,
    user: User,
    avatar: UploadFile,
):
    updated_user_dict = updated_user_schema.dict(exclude_unset=True)
    if updated_user_dict.get("password"):
        user.set_password(password=updated_user_dict.pop("password"))
    updated_user = parse_obj_to_model(user, updated_user_dict)
    if avatar:
        if updated_user.avatar:
            delete_file(updated_user.avatar, settings)
        saved_filename = await save_file(avatar, settings)
        updated_user.avatar = saved_filename

    db.commit()

    return updated_user


def create_grade(
    db: Session, grade: UserGradeCreateSchema, tenant_id: int, reviewer_id: int
):
    grade = UserGrade(grade=grade.grade, tenant_id=tenant_id, reviewer_id=reviewer_id)
    db.add(grade)
    db.commit()
    return grade


def get_messages(db: Session, user_1: int, user_2: int) -> List[Message]:
    messages = (
        db.query(Message)
        .filter(
            Message.sender_id.in_((user_1, user_2))
            & Message.receiver_id.in_((user_1, user_2))
        )
        .all()
    )
    return messages


def create_review(
    db: Session, review: UserReviewCreateSchema, tenant_id: int, reviewer_id: int
) -> UserReview:
    try:
        get_user_by_id(db, tenant_id)
    except NoResultFound:
        raise ModelNotFoundError("User")
    review = UserReview(text=review.text, tenant_id=tenant_id, reviewer_id=reviewer_id)
    db.add(review)
    db.commit()
    return review


def get_review_by_user(db: Session, user_id) -> List[UserReview]:
    reviews = db.query(UserReview).filter(UserReview.tenant_id == user_id).all()
    return reviews
