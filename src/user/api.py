from json import JSONDecodeError
from typing import List

from pydantic import ValidationError
from sqlalchemy.ext.asyncio import AsyncSession
from starlette.requests import Request
from sqlalchemy.exc import NoResultFound, IntegrityError
from sqlalchemy.orm import Session
from fastapi import WebSocket, WebSocketDisconnect, UploadFile, Form

from src.exceptions import UnauthorizedError, ModelNotFoundError
from .permissions import UserPrincipals, user_acl, user_review_acl, user_grade_acl
from src.settings import get_settings, Settings
from src.user.WebSocketManager import SocketManager
from src.user.models import User
from src.user.schema import (
    RegisterSchema,
    TokenResponseSchema,
    UserUpdateSchema,
    UserGradeCreateSchema,
    UserGradeResponseSchema,
    MessageResponseSchema,
    LoginSchema,
    UserReviewResponseSchema,
    UserReviewCreateSchema,
    UserResponseSchema,
)
from src.user.services import (
    create_text_message,
    get_user_by_id,
    verify_credentials,
    user_registration,
    edit_user,
    get_user_by_email,
    oauth2_registration,
    create_grade,
    get_messages,
    create_review,
    get_review_by_user,
)
from fastapi.security import OAuth2PasswordRequestForm
from src.db import get_db
from fastapi import APIRouter, Depends, HTTPException, status
from passlib.exc import InvalidTokenError
from src.user.jwt import create_access_token, verify_token
from src.utils.utils import check_image
from oauth2client import client
from oauth2client.client import FlowExchangeError


router = APIRouter(tags=["Users"], prefix="")


@router.post("/register", response_model=UserResponseSchema)
async def register(request: RegisterSchema, database: AsyncSession = Depends(get_db)):
    try:
        new_user = await user_registration(database, request)
    except IntegrityError:
        raise HTTPException(
            status_code=400,
            detail="User already exists.",
        )
    return new_user


@router.post("/login", response_model=TokenResponseSchema)
async def login(
    payload: OAuth2PasswordRequestForm = Depends(),
    db: Session = Depends(get_db),
    settings: Settings = Depends(get_settings),
):
    try:
        LoginSchema.from_orm(payload)
        user = verify_credentials(db, payload)
    except (NoResultFound, InvalidTokenError, ValidationError):
        raise UnauthorizedError
    token = create_access_token(settings, data={"user_id": user.id})
    return TokenResponseSchema(access_token=token)


@router.post("/oauth2", response_model=TokenResponseSchema)
async def swap_token(
    request: Request,
    db: AsyncSession = Depends(get_db),
    settings: Settings = Depends(get_settings),
):
    if not request.headers.get("X-Requested-With"):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="Incorrect headers"
        )
    try:
        code = await request.json()
        credentials = client.credentials_from_code(
            settings.GOOGLE_OAUTH2_CLIENT_ID, settings.GOOGLE_OAUTH2_CLIENT_SECRET, ["profile", "email"], code
        )
    except (FlowExchangeError, JSONDecodeError):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="Invalid code."
        )

    user = get_user_by_email(db, credentials.id_token["email"])

    if not user:
        user = oauth2_registration(db, credentials.id_token["email"])

    token = create_access_token(settings, data={"id": user.id})
    return TokenResponseSchema(access_token=token)


@router.get(
    "/profile/{user_id}",
    response_model=UserResponseSchema,
    dependencies=[Depends(verify_token)],
)
async def get_profile(user_id: int, db: Session = Depends(get_db)):
    user = get_user_by_id(db, user_id)
    if not user:
        raise ModelNotFoundError("User")
    return user


@router.patch("/profile/{user_id}", response_model=UserResponseSchema)
async def edit_profile(
    user_id: int,
    _=UserPrincipals("edit", user_acl),
    avatar: UploadFile = Form(""),
    updated_user_data: UserUpdateSchema = Form(None),
    user: User = Depends(verify_token),
    db: Session = Depends(get_db),
    settings: Settings = Depends(get_settings),
):
    if avatar:
        check_image(avatar.filename)
    try:
        updated_user = await edit_user(db, settings, updated_user_data, user, avatar)
    except IntegrityError:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="user with these email or phone number already exists.",
        )
    return updated_user


manager = SocketManager()


@router.websocket("/chat/{receiver_id}")
async def chat(
    websocket: WebSocket,
    receiver_id: int,
    token: str,
    settings: Settings = Depends(get_settings),
    db: Session = Depends(get_db),
):
    user = verify_token(db, token, settings)
    await manager.connect(websocket, user.id)
    try:
        while True:
            data = await websocket.receive_json()
            await manager.send_message(data, receiver_id)
            create_text_message(db, data["message"], user.id, receiver_id)
    except WebSocketDisconnect:
        manager.disconnect(user.id)


@router.post("/profile/{user_id}/grade", response_model=UserGradeResponseSchema)
async def new_grade(
    grade: UserGradeCreateSchema,
    user_id: int,
    _=UserPrincipals("create", user_grade_acl),
    reviewer: User = Depends(verify_token),
    db: Session = Depends(get_db),
):
    try:
        created_grade = create_grade(db, grade, user_id, reviewer.id)
    except IntegrityError:
        raise ModelNotFoundError("User")
    return created_grade


@router.get("/chat/{receiver_id}", response_model=List[MessageResponseSchema])
async def list_messages(
    receiver_id: int, db: Session = Depends(get_db), user_me: User = Depends(verify_token)
):
    messages = get_messages(db, user_me.id, receiver_id)
    return messages


@router.post(path="/profile/{user_id}/review", response_model=UserReviewResponseSchema)
async def create_user_review(
    user_id: int,
    ad: UserReviewCreateSchema,
    _=UserPrincipals("create", user_review_acl),
    reviewer: User = Depends(verify_token),
    db: Session = Depends(get_db),
):
    try:
        new_review = create_review(db, ad, user_id, reviewer.id)
    except IntegrityError:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Tenant not found.",
        )
    return new_review


@router.get(
    "/profile/{user_id}/review",
    response_model=List[UserReviewResponseSchema],
    dependencies=[Depends(verify_token)],
)
async def list_user_review(user_id: int, db: Session = Depends(get_db)):
    reviews = get_review_by_user(db, user_id)
    return reviews
