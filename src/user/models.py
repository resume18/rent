from sqlalchemy.orm import relationship, backref
from sqlalchemy import String, Integer, Boolean, ForeignKey, Column, Index

from .hashing import get_password_hash, verify_password
from ..model_bases import Base


class User(Base):
    __tablename__ = "user"

    first_name = Column(String(20))
    second_name = Column(String(20))
    last_name = Column(String(20))
    password = Column(String(100))
    avatar = Column(String(300))
    username = Column(String(20), unique=True)
    email = Column(String(100), unique=True)
    is_superuser = Column(Boolean, default=False)
    is_staff = Column(Boolean, default=False)

    def check_password(self, password):
        return verify_password(self.password, password)


class UserGrade(Base):
    __tablename__ = "user_rating"

    tenant_id = Column(
        Integer,
        ForeignKey("user.id", ondelete="CASCADE", onupdate="CASCADE"),
        nullable=False,
    )
    reviewer_id = Column(
        Integer,
        ForeignKey("user.id", ondelete="CASCADE", onupdate="CASCADE"),
        nullable=False,
    )
    grade = Column(Boolean, nullable=False)

    tenant = relationship(
        "User",
        backref=backref("grade_for_me", cascade="all, delete-orphan, save-update"),
        foreign_keys=[tenant_id],
    )
    reviewer = relationship(
        "User",
        backref=backref("grade_for_you", cascade="all, delete-orphan, save-update"),
        foreign_keys=[reviewer_id],
    )


class UserReview(Base):
    __tablename__ = "user_review"

    text = Column(String(500), nullable=False)
    tenant_id = Column(
        Integer,
        ForeignKey("user.id", ondelete="CASCADE", onupdate="CASCADE"),
        nullable=False,
    )
    reviewer_id = Column(
        Integer,
        ForeignKey("user.id", ondelete="CASCADE", onupdate="CASCADE"),
        nullable=False,
    )

    tenant = relationship(
        "User",
        backref=backref("review_for_me", cascade="all, delete-orphan, save-update"),
        foreign_keys=[tenant_id],
    )
    reviewer = relationship(
        "User",
        backref=backref(
            "review_for_you", cascade="all, delete-orphan, save-update", lazy="dynamic"
        ),
        foreign_keys=[reviewer_id],
    )


class Message(Base):
    __tablename__ = "message"

    text = Column(String(500), nullable=False)
    sender_id = Column(
        Integer,
        ForeignKey("user.id", ondelete="CASCADE", onupdate="CASCADE"),
        nullable=False,
    )
    receiver_id = Column(
        Integer,
        ForeignKey("user.id", ondelete="CASCADE", onupdate="CASCADE"),
        nullable=False,
    )

    sender = relationship(
        "User",
        backref=backref("message_for_you", cascade="all, delete-orphan, save-update"),
        foreign_keys=[sender_id],
    )
    receiver = relationship(
        "User",
        backref=backref("message_for_me", cascade="all, delete-orphan, save-update"),
        foreign_keys=[receiver_id],
    )


message_sender_index = Index("message_sender_index", Message.sender_id)
message_receiver_index = Index("message_receiver_index", Message.receiver_id)
review_tenant_index = Index("review_tenant_index", UserReview.tenant_id)
grade_tenant_index = Index("grade_tenant_index", UserGrade.tenant_id)
user_phone_number_index = Index("user_phone_number_index", User.username)
