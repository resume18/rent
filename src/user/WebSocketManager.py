from typing import Dict

from fastapi.websockets import WebSocket


class SocketManager:
    def __init__(self):
        self.active_connections: Dict[int, WebSocket] = {}

    async def connect(self, websocket: WebSocket, sender_id):
        await websocket.accept()
        self.active_connections[sender_id] = websocket

    def disconnect(self, user_id: int):
        del self.active_connections[user_id]

    async def send_message(self, data: dict, receiver_id: int):
        if receiver_id in self.active_connections:
            await self.active_connections[receiver_id].send_json(data)
