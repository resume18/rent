from typing import Optional

from fastapi import HTTPException
from phonenumbers import NumberParseException
from pydantic.networks import EmailStr
from pydantic import validator, constr, BaseModel
from starlette import status

from ..schema_bases import (
    BaseModelWithORM,
    AllOptionalMeta,
    sqlalchemy_to_pydantic,
    BaseWithReadDumpedJSON,
)
import phonenumbers
from .models import User, UserGrade, Message, UserReview
from ..settings import get_settings
from ..utils.utils import get_media_uri


def validate_phone_number(v):
    try:
        pn = phonenumbers.parse(v)
    except NumberParseException:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="Invalid phone number."
        )
    assert phonenumbers.is_valid_number(pn) is True
    return v


class RegisterSchema(BaseModel):
    username: str
    password: constr(min_length=6)

    _normalize_username = validator("username")(validate_phone_number)


class LoginSchema(RegisterSchema, BaseModelWithORM):
    pass


BaseUserUpdateSchema = sqlalchemy_to_pydantic(
    User,
    model_name="BaseUserUpdateSchema",
    exclude=("id", "is_staff", "avatar", "is_superuser", "created_at", "updated_at"),
)


class UserUpdateSchema(
    BaseWithReadDumpedJSON,
    RegisterSchema,
    BaseUserUpdateSchema,
    metaclass=AllOptionalMeta,
):
    email: Optional[EmailStr]


BaseUserResponseSchema = sqlalchemy_to_pydantic(
    User, model_name="BaseUserSchema", config=BaseModelWithORM.Config
)


class UserResponseSchema(BaseUserResponseSchema, metaclass=AllOptionalMeta):
    @validator("avatar")
    def perform_filename_to_path(cls, v):
        if v:
            settings = get_settings()
            v = get_media_uri(v, settings)
        return v


class TokenResponseSchema(BaseModelWithORM):
    access_token: str
    token_type: str = "bearer"


UserReviewCreateSchema = sqlalchemy_to_pydantic(
    db_model=UserReview, model_name="ReviewCreateSchema", include=("text",)
)


UserReviewResponseSchema = sqlalchemy_to_pydantic(
    db_model=UserReview,
    model_name="UserReviewResponseSchema",
    config=BaseModelWithORM.Config,
)


UserGradeResponseSchema = sqlalchemy_to_pydantic(
    db_model=UserGrade,
    model_name="UserGradeResponseSchema",
    config=BaseModelWithORM.Config,
)


UserGradeCreateSchema = sqlalchemy_to_pydantic(
    db_model=UserGrade, model_name="UserGradeCreateSchema", include=("grade",)
)


MessageResponseSchema = sqlalchemy_to_pydantic(
    Message, model_name="MessageResponseSchema", config=BaseModelWithORM.Config
)
