from typing import Optional

from fastapi import Depends
from fastapi.security.oauth2 import OAuth2, OAuthFlowsModel, OAuth2PasswordBearer
from datetime import datetime, timedelta

from fastapi.security.utils import get_authorization_scheme_param
from jose import JWTError, jwt
from sqlalchemy.orm import Session
from starlette.requests import Request

from src.user.models import User
from src.user.services import get_user_by_id
from src.db import get_db
from src.exceptions import UnauthorizedError
from src.settings import get_settings, Settings


class OAuth2PasswordBearerCookie(OAuth2):
    def __init__(
        self,
        tokenUrl: str,
        scheme_name: str = None,
        scopes: dict = None,
        auto_error: bool = True,
    ):
        if not scopes:
            scopes = {}
        flows = OAuthFlowsModel(password={"tokenUrl": tokenUrl, "scopes": scopes})
        super().__init__(flows=flows, scheme_name=scheme_name, auto_error=auto_error)

    async def __call__(self, request: Request) -> Optional[str]:
        authorization: str = request.headers.get("Authorization")
        if not authorization:
            authorization: str = request.cookies.get("Authorization")

        scheme, token = get_authorization_scheme_param(authorization)

        if scheme.lower() != "bearer":
            if self.auto_error:
                raise UnauthorizedError
            else:
                return None
        return token


oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/login")


def create_access_token(settings: Settings, data: dict) -> str:
    to_encode = data.copy()
    expire = datetime.utcnow() + timedelta(days=settings.ACCESS_TOKEN_EXPIRES_DAYS)
    to_encode["exp"] = expire
    encoded_jwt = jwt.encode(
        to_encode, settings.GOOGLE_OAUTH2_CLIENT_SECRET, algorithm=settings.PASSWORD_ENCRYPTION_ALGORITHM
    )
    return encoded_jwt


def parse_token(settings: Settings, token: str) -> dict:
    try:
        payload = jwt.decode(
            token, settings.GOOGLE_OAUTH2_CLIENT_SECRET, algorithms=settings.PASSWORD_ENCRYPTION_ALGORITHM
        )
    except JWTError:
        raise UnauthorizedError
    return payload


def verify_token(
    db: Session = Depends(get_db),
    token: str = Depends(oauth2_scheme),
    settings: Settings = Depends(get_settings),
) -> User:
    payload = parse_token(settings, token)
    if datetime.fromtimestamp(payload["exp"]) < datetime.now():
        raise UnauthorizedError
    user = get_user_by_id(db, payload["user_id"])
    if not user:
        raise UnauthorizedError
    return user
