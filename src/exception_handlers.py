from starlette import status
from starlette.requests import Request
from starlette.responses import JSONResponse

from src.exceptions import ModelNotFoundError
from src.main import app


@app.exception_handler(ModelNotFoundError)
async def model_not_found_exception_handler(request: Request, exc: ModelNotFoundError):
    return JSONResponse(
        status_code=status.HTTP_404_NOT_FOUND,
        content={"message": f"{exc.model_name} not found."},
    )
