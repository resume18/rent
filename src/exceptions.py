from fastapi import HTTPException
from starlette import status


UnauthorizedError = HTTPException(
    status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid credentials or token type."
)


class ModelNotFoundError(Exception):
    def __init__(self, model_name: str):
        self.model_name = model_name
