from fastapi import FastAPI
from uvicorn import run

from src.user.api import router as user_router
from src.rent.api import router as rent_router
from src.settings import get_settings
from src.serve_files.get_file import router as file_router

app = FastAPI()

settings = get_settings()

if settings.SERVE_FILES_LOCALLY:
    app.include_router(file_router)

app.include_router(user_router)
app.include_router(rent_router)


if __name__ == "__main__":
    run(
        "main:app",
        host=settings.APP_HOST,
        port=settings.APP_PORT,
        log_level="info",
        reload=True,
    )
