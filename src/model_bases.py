from sqlalchemy import Column, DateTime, func, Integer

from src.db import Base as SQLAlchemyBase


class Base(SQLAlchemyBase):
    __abstract__ = True

    id = Column(Integer, primary_key=True, autoincrement=True)
    created_at = Column(DateTime, server_default=func.now())
    updated_at = Column(DateTime, onupdate=func.now())
