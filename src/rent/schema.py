from typing import Optional, List

from fastapi import Query
from .models import Ad, AdGrade, Tag, AdReview, TypeOfHousing, City, Country, AdMedia
from ..schema_bases import (
    BaseModelWithORM,
    sqlalchemy_to_pydantic,
    AllOptionalMeta,
    BaseWithReadDumpedJSON,
)
from ..settings import get_settings
from ..user.schema import UserResponseSchema
from ..utils.utils import get_media_uri
from pydantic import validator


BaseAdActionsSchema = sqlalchemy_to_pydantic(
    db_model=Ad,
    model_name="BaseAdActionsSchema",
    exclude=("updated_at", "created_at", "user_id", "id"),
)


class AdCreateSchema(BaseAdActionsSchema, BaseWithReadDumpedJSON):
    tag_id: List[int]


class AdPartialUpdateSchema(
    BaseAdActionsSchema, BaseWithReadDumpedJSON, metaclass=AllOptionalMeta
):
    delete_images_ids: Optional[List[int]]


BaseAdResponseSchema = sqlalchemy_to_pydantic(
    Ad,
    model_name="BaseAdResponseAdSchema",
    config=BaseModelWithORM.Config,
    exclude=("user_id",),
)


AdMediaResponseSchema = sqlalchemy_to_pydantic(
    AdMedia, model_name="AdMediaResponseSchema", config=BaseModelWithORM.Config
)


TagResponseSchema = sqlalchemy_to_pydantic(
    Tag, model_name="TagResponseSchema", config=BaseModelWithORM.Config
)


TypeOfHousingResponseSchema = sqlalchemy_to_pydantic(
    TypeOfHousing,
    model_name="TypeOfHousingResponseSchema",
    config=BaseModelWithORM.Config,
)


class AdResponseSchema(BaseAdResponseSchema):
    tag: List[TagResponseSchema]
    type: TypeOfHousingResponseSchema
    media: List[AdMediaResponseSchema]
    user: UserResponseSchema
    common_grade: int

    @validator("media", each_item=True)
    def perform_filename_to_path(cls, v):
        if v:
            settings = get_settings()
            v = get_media_uri(v.filename, settings)
        return v


class AdFiltersSchema:
    def __init__(
        self,
        city_id: Optional[int] = None,
        count_of_room: Optional[int] = None,
        count_of_bed: Optional[int] = None,
        tag_id: Optional[List[int]] = Query([]),
        type_id: Optional[List[int]] = Query([]),
    ):
        self.city_id = city_id
        self.count_of_room = count_of_room
        self.count_of_bed = count_of_bed
        self.tag_id = tag_id
        self.type_id = type_id


AdGradeResponseSchema = sqlalchemy_to_pydantic(
    AdGrade, model_name="AdGradeResponseSchema", config=BaseModelWithORM.Config
)


AdGradeCreateSchema = sqlalchemy_to_pydantic(
    AdGrade,
    model_name="AdGradeCreateSchema",
    include=("grade",),
    config=BaseModelWithORM.Config,
)


AdReviewResponseSchema = sqlalchemy_to_pydantic(
    AdReview, model_name="AdReviewResponseSchema", config=BaseModelWithORM.Config
)


AdReviewCreateSchema = sqlalchemy_to_pydantic(
    AdReview, model_name="AdReviewCreateSchema", include=("text",)
)


BaseCountryResponseSchema = sqlalchemy_to_pydantic(
    Country, model_name="CountryResponseSchema", config=BaseModelWithORM.Config
)


CityResponseSchema = sqlalchemy_to_pydantic(
    City,
    model_name="CityResponseSchema",
    config=BaseModelWithORM.Config,
    exclude=("country_id",),
)


class CountryResponseSchema(BaseCountryResponseSchema):
    city: List[CityResponseSchema]


class FilterValuesResponseSchema(BaseModelWithORM):
    country: List[CountryResponseSchema]
    type: List[TypeOfHousingResponseSchema]
    tag: List[TagResponseSchema]
