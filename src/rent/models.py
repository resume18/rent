from typing import List

from sqlalchemy.orm import relationship, backref
from sqlalchemy import (
    String,
    Integer,
    ForeignKey,
    Column,
    Table,
    Boolean,
    Index,
)
from ..model_bases import Base


class TypeOfHousing(Base):
    __tablename__ = "type_of_housing"

    type_name = Column(String(30), unique=True)
    image = Column(String(50))

    ad = relationship("Ad", cascade="all, delete-orphan, save-update")


ad_tag = Table(
    "ad_tag",
    Base.metadata,
    Column(
        "ad_id",
        ForeignKey("ad.id", onupdate="CASCADE", ondelete="CASCADE"),
        primary_key=True,
        nullable=False,
    ),
    Column(
        "tag_id",
        ForeignKey("tag.id", onupdate="CASCADE", ondelete="CASCADE"),
        primary_key=True,
        nullable=False,
    ),
)


class Ad(Base):
    __tablename__ = "ad"

    user_id = Column(
        Integer,
        ForeignKey("user.id", onupdate="CASCADE", ondelete="CASCADE"),
        nullable=False,
    )
    title = Column(String(50), nullable=False)
    type_id = Column(
        Integer, ForeignKey("type_of_housing.id", onupdate="CASCADE", ondelete="SET NULL")
    )
    address = Column(String(70), nullable=False)
    city_id = Column(
        Integer, ForeignKey("city.id", onupdate="CASCADE", ondelete="SET NULL")
    )
    count_of_room = Column(Integer, nullable=False)
    count_of_bed = Column(Integer, nullable=False)
    description = Column(String(500))

    type = relationship(
        "TypeOfHousing", back_populates="ad", foreign_keys=[type_id], lazy="joined"
    )
    tag = relationship("Tag", secondary=ad_tag, back_populates="ad", lazy="joined")
    user = relationship(
        "User",
        backref=backref("ad", cascade="all, delete-orphan, save-update"),
        lazy="joined",
    )
    media = relationship(
        "AdMedia", lazy="joined", cascade="all, delete-orphan, save-update"
    )
    grade: List = relationship("AdGrade", cascade="all, delete-orphan, save-update")
    review = relationship("AdReview", cascade="all, delete-orphan, save-update")
    city = relationship(
        "City", back_populates="ad", foreign_keys=[city_id], lazy="joined"
    )

    @property
    def common_grade(self):
        common_grade = len([grade.id for grade in self.grade if grade.grade]) - len(
            [grade.id for grade in self.grade if not grade.grade]
        )
        return common_grade


class Tag(Base):
    __tablename__ = "tag"

    name = Column(String(20), unique=True)

    ad = relationship("Ad", secondary=ad_tag, back_populates="tag")


class AdReview(Base):
    __tablename__ = "ad_review"

    text = Column(String(500), nullable=False)
    ad_id = Column(
        Integer,
        ForeignKey("ad.id", onupdate="CASCADE", ondelete="CASCADE"),
        nullable=False,
    )
    tenant_id = Column(
        Integer,
        ForeignKey("user.id", onupdate="CASCADE", ondelete="CASCADE"),
        nullable=False,
    )

    tenant = relationship(
        "User",
        backref="ad_review",
        foreign_keys=[tenant_id],
    )
    ad = relationship("Ad", back_populates="review", foreign_keys=[ad_id])


class AdGrade(Base):
    __tablename__ = "ad_rating"

    ad_id = Column(
        Integer,
        ForeignKey("ad.id", onupdate="CASCADE", ondelete="CASCADE"),
        nullable=False,
    )
    tenant_id = Column(
        Integer,
        ForeignKey("user.id", onupdate="CASCADE", ondelete="CASCADE"),
        nullable=False,
    )
    grade = Column(Boolean, nullable=False)

    tenant = relationship(
        "User",
        backref="ad_grade",
        foreign_keys=[tenant_id],
    )
    ad = relationship("Ad", back_populates="grade", foreign_keys=[ad_id])


class AdMedia(Base):
    __tablename__ = "ad_media"

    ad_id = Column(
        Integer,
        ForeignKey("ad.id", ondelete="CASCADE", onupdate="CASCADE"),
        nullable=False,
    )
    filename = Column(String(300), nullable=False, unique=True)

    ad = relationship("Ad", back_populates="media", foreign_keys=[ad_id])


class Country(Base):
    __tablename__ = "country"

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(100), unique=True)

    city = relationship("City", cascade="all, delete-orphan, save-update", lazy="joined")


class City(Base):
    __tablename__ = "city"

    name = Column(String(100), unique=True)
    country_id = Column(
        Integer,
        ForeignKey("country.id", ondelete="CASCADE", onupdate="CASCADE"),
        nullable=False,
    )

    ad = relationship("Ad", back_populates="city", cascade="all, save-update")
    country = relationship("Country", back_populates="city", foreign_keys=[country_id])


media_ad_id_index = Index("media_ad_id_index", AdMedia.ad_id)
review_ad_id_index = Index("review_ad_id_index", AdReview.ad_id)
grade_ad_id_index = Index("grade_ad_id_index", AdGrade.ad_id)
ad_user_id = Index("ad_user_id", Ad.user_id)
ad_type_id_index = Index("ad_type_id_index", Ad.type_id)
ad_address_index = Index("ad_address_index", Ad.address)
ad_count_of_room_index = Index("ad_count_of_room_index", Ad.count_of_room)
ad_count_of_bed_index = Index("ad_count_of_bed_index", Ad.count_of_bed)
