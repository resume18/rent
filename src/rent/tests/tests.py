import json
from .factories import create_ad
from src.user.tests.tests import create_auth_user


def test_new_ad(client, db_session, tmp_path):
    user = create_auth_user(client)
    response = create_ad(client, db_session, tmp_path, user)
    assert response.status_code == 200


def test_get_ads(client):
    user = create_auth_user(client)
    response = client.get("/ad", headers=user["headers"])
    assert response.status_code == 200


def test_get_filters(client):
    user = create_auth_user(client)
    response = client.get("/ad/filters", headers=user["headers"])
    assert response.status_code == 200


def test_get_reviews(client, db_session, tmp_path):
    user = create_auth_user(client)
    response_ad = create_ad(client, db_session, tmp_path, user).json()
    response = client.get(f"/ad/{response_ad['id']}/review", headers=user["headers"])
    assert response.status_code == 200


def test_new_review(client, db_session, tmp_path):
    creator = create_auth_user(client)
    response_ad = create_ad(client, db_session, tmp_path, creator).json()
    data = {"text": "sdfsdffsdf"}
    reviewer = create_auth_user(client)
    response = client.post(
        f"/ad/{response_ad['id']}/review",
        data=json.dumps(data),
        headers=reviewer["headers"],
    )
    assert response.status_code == 200


def test_delete_ad(client, tmp_path, db_session):
    user = create_auth_user(client)
    ad = create_ad(client, db_session, tmp_path, user).json()
    response = client.delete(f"/ad/{ad['id']}", headers=user["headers"])
    assert response.status_code == 200


def test_update_ad(client, tmp_path, db_session):
    user = create_auth_user(client)
    ad = create_ad(client, db_session, tmp_path, user).json()
    data = {"updated_ad": json.dumps({"count_of_room": ad["count_of_room"] + 10})}
    response = client.patch(f"/ad/{ad['id']}", data, headers=user["headers"])
    print(response.json())
    assert response.status_code == 200
    assert ad["count_of_room"] != response.json()["count_of_room"]


def test_new_grade(client, tmp_path, db_session):
    user = create_auth_user(client)
    grader = create_auth_user(client)
    ad = create_ad(client, db_session, tmp_path, user).json()
    data = {"grade": True}
    response = client.post(
        f"/ad/{ad['id']}/grade", json.dumps(data), headers=grader["headers"]
    )
    assert response.status_code == 200
