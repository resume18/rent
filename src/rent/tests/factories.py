import json

from starlette.testclient import TestClient

from src.rent.models import TypeOfHousing, Tag, Country, City


def create_ad(client: TestClient, db_session, tmp_path, auth_user):
    type_of_housing = TypeOfHousing(type_name="qqqqqqq")
    tag = Tag(name="tag1")
    country = Country(name="aaaaaaa")
    db_session.add(type_of_housing)
    db_session.add(tag)
    db_session.add(country)
    db_session.commit()
    city = City(name="asdasd", country_id=country.id)
    db_session.add(city)
    db_session.commit()
    file = tmp_path / "test.png"
    file.touch()
    file.write_text("sadasdassadad")
    data = {
        "ad": json.dumps(
            {
                "title": "asdad",
                "city_id": city.id,
                "type_id": type_of_housing.id,
                "tag_id": [tag.id],
                "description": "asdasdasdasd",
                "count_of_room": 2,
                "count_of_bed": 4,
                "address": "asdasdasdsa",
            }
        )
    }
    response = client.post(
        "/ad",
        data=data,
        files={"images": ("a.png", open(file, "r+b"), "multipart/form-data")},
        headers=auth_user["headers"],
    )
    return response
