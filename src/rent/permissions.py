from fastapi import Depends
from fastapi_permissions import Allow, Deny, Authenticated

from src.db import get_db
from src.rent.services import get_ad_by_id


def ad_acl(ad_id: int, db=Depends(get_db)):
    ad = get_ad_by_id(db, ad_id)
    return [
        (Allow, f"user:{ad.user_id}", "delete"),
        (Allow, f"user:{ad.user_id}", "edit"),
    ]


def ad_grade_acl(ad_id, db=Depends(get_db)):
    ad = get_ad_by_id(db, ad_id)
    return [(Deny, f"user:{ad.user_id}", "create"), (Allow, Authenticated, "create")]


def ad_review_acl(ad_id, db=Depends(get_db)):
    ad = get_ad_by_id(db, ad_id)
    return [(Deny, f"user:{ad.user_id}", "create"), (Allow, Authenticated, "create")]
