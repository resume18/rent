from typing import List
from fastapi import UploadFile
from sqlalchemy.exc import NoResultFound
from sqlalchemy.orm import Session

from src.exceptions import ModelNotFoundError
from ..settings import Settings
from ..user.models import User
from ..utils.utils import save_file, delete_file, parse_obj_to_model
from .schema import (
    AdCreateSchema,
    AdPartialUpdateSchema,
    AdGradeCreateSchema,
    AdFiltersSchema,
)
from src.rent.models import Ad, AdMedia, TypeOfHousing, Tag, AdGrade, City, AdReview, Country


def get_all_type_of_housing(db: Session) -> List[TypeOfHousing]:
    h_type = db.query(TypeOfHousing).all()
    return h_type


def get_tags_by_ids(db: Session, tag_ids: List[int]) -> List[Tag]:
    tags = db.query(Tag).filter(Tag.id.in_(tag_ids)).all()
    if tag_ids != [i.id for i in tags]:
        raise ModelNotFoundError("Tag")
    return tags


def get_type_of_housing_by_id(db, type_id):
    try:
        type = db.query(TypeOfHousing).filter_by(id=type_id).one()
    except NoResultFound:
        raise ModelNotFoundError("Type")
    return type


def get_city_by_id(db: Session, city_id: int):
    try:
        city = db.query(City).filter_by(id=city_id).one()
    except NoResultFound:
        raise ModelNotFoundError("City")
    return city


async def create_ad(
    db: Session,
    settings: Settings,
    images: List[UploadFile],
    ad: AdCreateSchema,
    user: User,
) -> Ad:
    tags = get_tags_by_ids(db, ad.tag_id)
    type_of_housing = get_type_of_housing_by_id(db, ad.type_id)
    city = get_city_by_id(db, ad.city_id)

    new_ad = Ad(
        title=ad.title,
        description=ad.description,
        address=ad.address,
        count_of_bed=ad.count_of_bed,
        count_of_room=ad.count_of_room,
        tag=tags,
        type=type_of_housing,
        user_id=user.id,
        city=city,
    )
    db.add(new_ad)
    db.commit()

    await create_ad_medias(db, settings, new_ad.id, images)

    return new_ad


async def create_ad_medias(
    db: Session, settings: Settings, ad_id: int, images: List[UploadFile]
):
    for image in images:
        path = await save_file(image, settings)
        db.add(AdMedia(filename=path, ad_id=ad_id))

    db.commit()


def get_ad_by_id(db: Session, ad_id: int) -> Ad:
    try:
        ad = db.query(Ad).filter(Ad.id == ad_id).one()
    except NoResultFound:
        raise ModelNotFoundError("Ad")
    return ad


def delete_ad_obj(db: Session, ad: Ad, settings: Settings) -> None:
    for item in ad.media:
        delete_file(item.filename, settings)
    db.delete(ad)
    db.commit()


async def update_ad(
    db: Session,
    settings: Settings,
    ad: Ad,
    updated_ad_schema: AdPartialUpdateSchema,
    images: List[UploadFile],
) -> Ad:
    updated_ad_dict = updated_ad_schema.dict(exclude_unset=True)
    # copy list because we remove items from list itself
    if updated_ad_dict.get("delete_images_ids"):
        if not updated_ad_dict["delete_images_ids"] in [image.id for image in ad.media]:
            raise ModelNotFoundError("images for delete")

    if updated_ad_dict.get("delete_images_ids"):
        for image in ad.media:
            if image.id in updated_ad_dict["delete_images_ids"]:
                ad.media.remove(image)

    if updated_ad_dict.get("tag_id"):
        ad.tag = get_tags_by_ids(db, updated_ad_dict.pop("tag_id"))
    if updated_ad_dict.get("type_id"):
        ad.type_of_housing = get_type_of_housing_by_id(db, updated_ad_dict.pop("type_id"))
    if updated_ad_dict.get("city_id"):
        ad.city = get_city_by_id(db, updated_ad_dict.pop("city_id"))

    updated_ad = parse_obj_to_model(ad, updated_ad_dict)

    await create_ad_medias(db, settings, ad.id, images)

    db.commit()
    for image in ad.media[:]:
        delete_file(image.filename, settings)
    return updated_ad


def create_grade(
    db: Session, ad_id: int, grade: AdGradeCreateSchema, user_id: int
) -> AdGrade:
    grade = AdGrade(ad_id=ad_id, grade=grade.grade, tenant_id=user_id)
    db.add(grade)
    db.commit()
    return grade


def get_filtered_ads(db: Session, filters: AdFiltersSchema) -> List[Ad]:
    cleared_filters = {key: value for key, value in filters.__dict__.items() if value}
    related_models_filters = []
    if "tag_id" in cleared_filters:
        related_models_filters.append(Tag.id.in_(cleared_filters.pop("tag_id")))
    if "type_id" in cleared_filters:
        related_models_filters.append(Ad.type_id.in_(cleared_filters.pop("type_id")))
    ads = (
        db.query(Ad)
        .filter_by(**cleared_filters)
        .join(Ad.tag)
        .filter(*related_models_filters)
        .all()
    )
    return ads


def get_all_tags(db: Session) -> List[Tag]:
    tags = db.query(Tag).all()
    return tags


def create_review(
    db: Session, ad_id: int, reviewer_id: int, review: AdReview
) -> AdReview:
    review = AdReview(ad_id=ad_id, text=review.text, tenant_id=reviewer_id)
    db.add(review)
    db.commit()
    return review


def get_reviews_by_ad_id(db: Session, ad_id: int) -> List[AdReview]:
    return db.query(AdReview).filter(AdReview.ad_id == ad_id).all()


def get_countries(db: Session):
    return db.query(Country).join(City).all()
