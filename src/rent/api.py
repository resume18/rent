from typing import List
from sqlalchemy.orm import Session
from starlette import status
from fastapi.responses import JSONResponse

from .permissions import ad_acl, ad_grade_acl, ad_review_acl
from .schema import (
    AdCreateSchema,
    AdResponseSchema,
    AdPartialUpdateSchema,
    AdGradeCreateSchema,
    AdGradeResponseSchema,
    AdFiltersSchema,
    AdReviewResponseSchema,
    AdReviewCreateSchema,
    FilterValuesResponseSchema,
)
from .services import (
    create_ad,
    get_ad_by_id,
    delete_ad_obj,
    update_ad,
    create_grade,
    get_filtered_ads,
    get_all_tags,
    get_reviews_by_ad_id,
    create_review,
    get_all_type_of_housing,
    get_countries,
)
from ..db import get_db
from fastapi import APIRouter, Depends, UploadFile, File, Form

from ..settings import get_settings, Settings
from ..user.permissions import UserPrincipals
from ..utils.utils import check_image
from ..user.jwt import verify_token
from ..user.models import User

router = APIRouter(tags=["Rent"], prefix="/ad")


@router.post(path="", response_model=AdResponseSchema)
async def new_ad(
    images: List[UploadFile],
    ad: AdCreateSchema = Form(None),
    user: User = Depends(verify_token),
    db: Session = Depends(get_db),
    settings: Settings = Depends(get_settings),
):
    created_ad = await create_ad(db, settings, images, ad, user)
    return created_ad


@router.delete(path="/{ad_id}")
async def delete_ad(
    ad_id,
    _=UserPrincipals("delete", ad_acl),
    db: Session = Depends(get_db),
    settings: Settings = Depends(get_settings),
):
    ad = get_ad_by_id(db, ad_id)
    delete_ad_obj(db, ad, settings)
    return JSONResponse(status_code=status.HTTP_200_OK)


@router.patch("/{ad_id}", response_model=AdResponseSchema)
async def partial_update_ad(
    ad_id: int,
    _=UserPrincipals("edit", ad_acl),
    images: List[UploadFile] = File([]),
    updated_ad: AdPartialUpdateSchema = Form(None),
    db: Session = Depends(get_db),
    settings: Settings = Depends(get_settings),
):
    for image in images:
        check_image(image.filename)
    ad = get_ad_by_id(db, ad_id)

    if not updated_ad.dict(exclude_unset=True, exclude_none=True):
        return ad

    result = await update_ad(db, settings, ad, updated_ad, images)

    return result


@router.get(
    "", response_model=List[AdResponseSchema], dependencies=[Depends(verify_token)]
)
async def get_list_ad(
    db: Session = Depends(get_db), filters: AdFiltersSchema = Depends(AdFiltersSchema)
):
    ads = get_filtered_ads(db, filters)
    return ads


@router.post("/{ad_id}/grade", response_model=AdGradeResponseSchema)
async def new_grade(
    ad_id: int,
    grade: AdGradeCreateSchema,
    user: User = Depends(verify_token),
    db: Session = Depends(get_db),
    _=UserPrincipals("create", ad_grade_acl),
):
    created_grade = create_grade(db, ad_id, grade, user.id)
    return created_grade


@router.post("/{ad_id}/review", response_model=AdReviewResponseSchema)
async def new_review(
    ad_id: int,
    review: AdReviewCreateSchema,
    user: User = Depends(verify_token),
    db: Session = Depends(get_db),
    _=UserPrincipals("create", ad_review_acl),
):
    created_review = create_review(db, ad_id, user.id, review)
    return created_review


@router.get(
    "/{ad_id}/review",
    response_model=List[AdReviewResponseSchema],
    dependencies=[Depends(verify_token)],
)
async def get_reviews(ad_id: int, db: Session = Depends(get_db)):
    reviews = get_reviews_by_ad_id(db, ad_id)
    return reviews


@router.get(
    "/filters",
    dependencies=[Depends(verify_token)],
    response_model=FilterValuesResponseSchema,
)
async def get_filters(db: Session = Depends(get_db)):
    types = get_all_type_of_housing(db)
    tags = get_all_tags(db)
    countries = get_countries(db)
    return FilterValuesResponseSchema(tag=tags, type=types, country=countries)
