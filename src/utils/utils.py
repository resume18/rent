import os
import string
import random

import aiofiles
from fastapi import HTTPException, UploadFile
from starlette import status

from src.db import Base
from src.settings import Settings


def check_image(filename: str) -> None:
    is_image = filename.lower().endswith(
        (".png", ".jpg", ".jpeg", ".tiff", ".bmp", ".gif")
    )
    if not is_image:
        raise HTTPException(
            detail="Not image detected.", status_code=status.HTTP_400_BAD_REQUEST
        )


def generate_hash(hash_length: int = 50) -> str:
    all_chars = string.ascii_letters + string.digits
    hash_str = ""
    for i in range(hash_length):
        hash_str += random.choice(all_chars)
    return hash_str


async def save_file(file: UploadFile, settings: Settings) -> str:
    file_name = f"{generate_hash()}.{file.filename.split('.')[-1]}"
    path = os.path.join(settings.BASE_DIR, settings.UPLOAD_DIR, file_name)
    async with aiofiles.open(path, "wb") as f:
        content = await file.read()
        await f.write(content)
    return file_name


def delete_file(file_name: str, settings: Settings) -> None:
    path = os.path.join(settings.BASE_DIR, settings.UPLOAD_DIR, file_name)
    os.remove(path)


def get_media_uri(file_name: str, settings: Settings) -> str:
    path = os.path.join(settings.APP_HOST, settings.UPLOAD_DIR, file_name)
    return path


def parse_obj_to_model(model: Base, json_model: dict) -> Base:
    for key, value in json_model.items():
        if hasattr(model, key):
            setattr(model, key, value)
    return model
