"""
If there is no nginx in front of fastapi
"""

import os.path

from fastapi import APIRouter
from starlette.responses import FileResponse

from src.settings import get_settings

settings = get_settings()

router = APIRouter(prefix=f"/{settings.UPLOAD_DIR}")


@router.get("/{filename:path}", include_in_schema=False)
def get_file(filename: str) -> FileResponse:
    return FileResponse(path=os.path.join(settings.UPLOAD_DIR, filename))
