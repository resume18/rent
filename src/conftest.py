import logging
from asyncio import current_task

import pytest
from sqlalchemy_utils import create_database, database_exists, drop_database
from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession, async_scoped_session
from typing import Any, Generator
from fastapi import FastAPI
from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from src.db import get_db, Base
from src.settings import get_settings
from src.user.api import router as user_router
from src.rent.api import router as rent_router
from src.serve_files.get_file import router as file_router


logging.disable(logging.CRITICAL)

settings = get_settings()

DB_URI = f"{settings.DSN__DB.split('/')[0]}/__TEST__"

if not database_exists(DB_URI):
    create_database(DB_URI)

async_engine = create_async_engine(DB_URI)
async_session = async_scoped_session(
    sessionmaker(
        bind=async_engine, autoflush=False, autocommit=False, _class=AsyncSession, expire_on_commit=False
    ),
    current_task
)


async def get_test_db():
    db = async_session()
    try:
        yield db
    finally:
        await db.close()


engine = create_engine(DB_URI)
SessionTesting = sessionmaker(autocommit=False, autoflush=False, bind=engine)


def start_application():
    app = FastAPI()
    app.dependency_overrides[get_db] = get_test_db
    app.include_router(user_router)
    app.include_router(rent_router)
    app.include_router(file_router)
    return app


@pytest.fixture(scope="session", autouse=True)
def app() -> Generator[FastAPI, Any, None]:
    Base.metadata.create_all(engine)
    _app = start_application()
    yield _app
    drop_database(DB_URI)


@pytest.fixture(scope="function")
def db_session(app: FastAPI) -> Generator[SessionTesting, Any, None]:
    connection = engine.connect()
    session = SessionTesting(bind=connection)
    yield session
    session.close()


@pytest.fixture(scope="function")
def client(app: FastAPI, db_session: SessionTesting) -> Generator[TestClient, Any, None]:
    """
    Create a new FastAPI TestClient that uses the `db_session` fixture to override
    the `get_db` dependency that is injected into routes.
    """

    def _get_test_db():
        try:
            yield db_session
        finally:
            pass

    app.dependency_overrides[get_db] = _get_test_db
    with TestClient(app) as client:
        yield client
