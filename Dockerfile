FROM python:3.8

ENV PYTHONUNBUFFERED 1

RUN addgroup --system user && adduser --system --group user

ENV PATH=/home/user/.local/bin:${PATH}

ENV APP_HOME=/app

WORKDIR ${APP_HOME}/

RUN chown -R user:user ${APP_HOME}

COPY --chown=user:user ./requirements.txt .

RUN pip install -r requirements.txt && poetry config virtualenvs.create false

COPY --chown=user:user pyproject.toml .
COPY --chown=user:user poetry.lock .

RUN poetry install

COPY --chown=user:user . .

CMD alembic upgrade head && \
    uvicorn src.main:app --host=${APP_HOST} --port=${APP_PORT} --proxy-headers

USER user