### The meaning of the project: Renting a home is quick and easy.

#### What will the project include?
____
The meaning of the project: Renting a home is quick and easy.

1. It will be possible to rent a room, apartment, house, hotel room.
2. Chat with the tenant / owner of housing, technical support.
3. In addition to search filters, there will be keywords by which the search will be determined.
4. Admin. (Full rights or only to moderate something)
5. User's personal account (account management, own ads).
6. Opportunity to let and rent a property.
7. Reviews, rating (likes), both about the tenants and the housing itself.
8. Perhaps there will be a basis for connecting acquiring. (it is assumed that the money will first go to a separate account until the tenant confirms that the ad on the site is true).
9. Recommendation system.
10. After the deal has expired, there will be a tenant and tenant rating page for the tenant and tenant, respectively.
____
https://www.figma.com/file/DCb07KX5R3PWlqy6VoqmH7/Untitled?node-id=0%3A1

### db
![er-db](https://sun7-7.userapi.com/s/v1/if2/zQMme1bwWGle0Phx2cqEJwlrTQNJx23xn5si0Ni9ULoJ1WbSiFbppe9a-zb6SXNSn_ZZRP2_-xAmYtDsV_ZvBz9V.jpg?size=1794x688&quality=95&type=album)
